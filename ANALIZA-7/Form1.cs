﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ANALIZA_7
{
    public partial class Form1 : Form
    {
        string znak_prvog_igraca;
        string znak_drugog_igraca;
        int prvivodi = 0;
        int drugivodi = 0;
        int nerijeseno = 0;
        int i = 0;
        int brojac = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_pokreni_Click(object sender, EventArgs e)
        {
            if (txtBox_ime_prvog.Text == "")
            {
                MessageBox.Show("Unesite ime prvog igrača!");
            }
            if (txtBox_ime_drugog.Text == "")
            {
                MessageBox.Show("Unesite ime drugog igrača!");
            }
            lbl_ime_prvog_ukupni.Text = txtBox_ime_prvog.Text;
            lbl_ime_drugog_ukupni.Text = txtBox_ime_drugog.Text;

            if (radiob_O.Checked)
            {
                znak_drugog_igraca = "X";
                znak_prvog_igraca = "O";
            }
            else if (radiob_X.Checked)
            {
                znak_drugog_igraca = "O";
                znak_prvog_igraca = "X";
            }
            for (i = 0; i < 9; i++)
            {
                if ((label1.Text == "X" && label4.Text == "X" && label7.Text == "X") || (label2.Text == "X" && label5.Text == "X" && label8.Text == "X") || (label3.Text == "X" && label6.Text == "X" && label9.Text == "X") || (label1.Text == "X" && label2.Text == "X" && label3.Text == "X") || (label4.Text == "X" && label5.Text == "X" && label6.Text == "X") || (label7.Text == "X" && label8.Text == "X" && label9.Text == "X") || (label1.Text == "X" && label5.Text == "X" && label9.Text == "X"))
                {
                    if (znak_prvog_igraca == "X")
                    {
                        nerijeseno++;
                        MessageBox.Show("Pobjedio/la je: "+txtBox_ime_prvog.Text+"!");
                        txtbox_ukupni_prvog.Text = nerijeseno.ToString();
                        break;
                        
                    }
                    else
                    {
                        nerijeseno++;
                        MessageBox.Show("Pobjedio/la je: " + txtBox_ime_drugog.Text + "!");
                        txtbox_ukupni_drugog.Text = nerijeseno.ToString();
                        break;
                    }
                }
                if ((label1.Text == "O" && label4.Text == "O" && label7.Text == "O") || (label2.Text == "O" && label5.Text == "O" && label8.Text == "O") || (label3.Text == "O" && label6.Text == "O" && label9.Text == "O") || (label1.Text == "O" && label2.Text == "O" && label3.Text == "O") || (label4.Text == "O" && label5.Text == "O" && label6.Text == "O") || (label7.Text == "O" && label8.Text == "O" && label9.Text == "O") || (label1.Text == "O" && label5.Text == "O" && label9.Text == "O"))
                {
                    if (znak_prvog_igraca == "0")
                    {
                        nerijeseno++;
                        MessageBox.Show("Pobjedio/la je: " + txtBox_ime_prvog.Text + "!");
                        txtbox_ukupni_prvog.Text = nerijeseno.ToString();
                        break;
                    }
                    else
                    {
                        nerijeseno++;
                        MessageBox.Show("Pobjedio/la je: " + txtBox_ime_drugog.Text + "!");
                        txtbox_ukupni_drugog.Text = nerijeseno.ToString();
                        break;
                    }
                }
                if (txtbox_izabranopolje.Text == "A1")
                {
                    label1.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "A2")
                {
                    label4.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "A3")
                {
                    label7.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "B1")
                {
                    label2.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "B2")
                {
                    label5.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "B3")
                {
                    label8.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "C1")
                {
                    label3.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "C2")
                {
                    label6.Text = "X";
                }
                else if (txtbox_izabranopolje.Text == "C3")
                {
                    label9.Text = "X";
                }
                brojac++;
                if (brojac % 2 != 0)
                {
                    if (txtbox_izabranopolje.Text == "A1")
                    {
                        label1.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "A2")
                    {
                        label4.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "A3")
                    {
                        label7.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "B1")
                    {
                        label2.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "B2")
                    {
                        label5.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "B3")
                    {
                        label8.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "C1")
                    {
                        label3.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "C2")
                    {
                        label6.Text = "O";
                    }
                    else if (txtbox_izabranopolje.Text == "C3")
                    {
                        label9.Text = "O";
                    }
                }

            }
            if (nerijeseno == 0 && i==8)
            {
                MessageBox.Show("Nerijeseno je!");
            }
        
    }

    private void lbl_ime_prvog_Click(object sender, EventArgs e)
    {

    }

    private void txtBox_ime_prvog_TextChanged(object sender, EventArgs e)
    {

    }

    private void radiob_X_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void radiob_O_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void lbl_ime_drugog_Click(object sender, EventArgs e)
    {

    }

    private void txtBox_ime_drugog_TextChanged(object sender, EventArgs e)
    {

    }

    private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
    {

    }

    private void label1_Click(object sender, EventArgs e)
    {

    }

    private void label2_Click(object sender, EventArgs e)
    {

    }

    private void label3_Click(object sender, EventArgs e)
    {

    }

    private void label4_Click(object sender, EventArgs e)
    {

    }

    private void label5_Click(object sender, EventArgs e)
    {

    }

    private void label6_Click(object sender, EventArgs e)
    {

    }

    private void label7_Click(object sender, EventArgs e)
    {

    }

    private void label8_Click(object sender, EventArgs e)
    {

    }

    private void label9_Click(object sender, EventArgs e)
    {

    }

    private void lbl_ukupni_rez_Click(object sender, EventArgs e)
    {

    }

    private void labelA_Click(object sender, EventArgs e)
    {

    }

    private void labelB_Click(object sender, EventArgs e)
    {

    }

    private void labelC_Click(object sender, EventArgs e)
    {

    }

    private void label_prvired_Click(object sender, EventArgs e)
    {

    }

    private void label_drugired_Click(object sender, EventArgs e)
    {

    }

    private void label_trecired_Click(object sender, EventArgs e)
    {

    }

    private void label_izaberipolje_Click(object sender, EventArgs e)
    {

    }

        private void btn_nova_igra_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
            label4.Text = "";
            label5.Text = "";
            label6.Text = "";
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
            txtbox_izabranopolje.Text = "";
        }

        private void lbl_ime_prvog_ukupni_Click(object sender, EventArgs e)
        {

        }

        private void txtbox_ukupni_prvog_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_ime_drugog_ukupni_Click(object sender, EventArgs e)
        {

        }

        private void txtbox_ukupni_drugog_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
