﻿namespace ANALIZA_7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ime_prvog = new System.Windows.Forms.Label();
            this.txtBox_ime_prvog = new System.Windows.Forms.TextBox();
            this.txtBox_ime_drugog = new System.Windows.Forms.TextBox();
            this.btn_pokreni = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_ime_drugog = new System.Windows.Forms.Label();
            this.lbl_ukupni_rez = new System.Windows.Forms.Label();
            this.radiob_X = new System.Windows.Forms.RadioButton();
            this.radiob_O = new System.Windows.Forms.RadioButton();
            this.label_prvired = new System.Windows.Forms.Label();
            this.label_drugired = new System.Windows.Forms.Label();
            this.label_trecired = new System.Windows.Forms.Label();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.label_izaberipolje = new System.Windows.Forms.Label();
            this.txtbox_izabranopolje = new System.Windows.Forms.TextBox();
            this.btn_nova_igra = new System.Windows.Forms.Button();
            this.txtbox_ukupni_prvog = new System.Windows.Forms.TextBox();
            this.txtbox_ukupni_drugog = new System.Windows.Forms.TextBox();
            this.lbl_ime_prvog_ukupni = new System.Windows.Forms.Label();
            this.lbl_ime_drugog_ukupni = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_ime_prvog
            // 
            this.lbl_ime_prvog.AutoSize = true;
            this.lbl_ime_prvog.Location = new System.Drawing.Point(3, 9);
            this.lbl_ime_prvog.Name = "lbl_ime_prvog";
            this.lbl_ime_prvog.Size = new System.Drawing.Size(88, 13);
            this.lbl_ime_prvog.TabIndex = 0;
            this.lbl_ime_prvog.Text = "ime prvog igraca:";
            this.lbl_ime_prvog.Click += new System.EventHandler(this.lbl_ime_prvog_Click);
            // 
            // txtBox_ime_prvog
            // 
            this.txtBox_ime_prvog.Location = new System.Drawing.Point(98, 2);
            this.txtBox_ime_prvog.Name = "txtBox_ime_prvog";
            this.txtBox_ime_prvog.Size = new System.Drawing.Size(100, 20);
            this.txtBox_ime_prvog.TabIndex = 2;
            this.txtBox_ime_prvog.TextChanged += new System.EventHandler(this.txtBox_ime_prvog_TextChanged);
            // 
            // txtBox_ime_drugog
            // 
            this.txtBox_ime_drugog.Location = new System.Drawing.Point(98, 30);
            this.txtBox_ime_drugog.Name = "txtBox_ime_drugog";
            this.txtBox_ime_drugog.Size = new System.Drawing.Size(100, 20);
            this.txtBox_ime_drugog.TabIndex = 3;
            this.txtBox_ime_drugog.TextChanged += new System.EventHandler(this.txtBox_ime_drugog_TextChanged);
            // 
            // btn_pokreni
            // 
            this.btn_pokreni.Location = new System.Drawing.Point(204, 77);
            this.btn_pokreni.Name = "btn_pokreni";
            this.btn_pokreni.Size = new System.Drawing.Size(75, 23);
            this.btn_pokreni.TabIndex = 4;
            this.btn_pokreni.Text = "unesi";
            this.btn_pokreni.UseVisualStyleBackColor = true;
            this.btn_pokreni.Click += new System.EventHandler(this.btn_pokreni_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(80, 155);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(355, 154);
            this.tableLayoutPanel1.TabIndex = 5;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 51);
            this.label1.TabIndex = 0;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(121, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 51);
            this.label2.TabIndex = 1;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(239, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 51);
            this.label3.TabIndex = 2;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 51);
            this.label4.TabIndex = 3;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(121, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 51);
            this.label5.TabIndex = 4;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(239, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 51);
            this.label6.TabIndex = 5;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 52);
            this.label7.TabIndex = 6;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(121, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 52);
            this.label8.TabIndex = 7;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(239, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 52);
            this.label9.TabIndex = 8;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // lbl_ime_drugog
            // 
            this.lbl_ime_drugog.AutoSize = true;
            this.lbl_ime_drugog.Location = new System.Drawing.Point(3, 37);
            this.lbl_ime_drugog.Name = "lbl_ime_drugog";
            this.lbl_ime_drugog.Size = new System.Drawing.Size(94, 13);
            this.lbl_ime_drugog.TabIndex = 6;
            this.lbl_ime_drugog.Text = "ime drugog igraca:";
            this.lbl_ime_drugog.Click += new System.EventHandler(this.lbl_ime_drugog_Click);
            // 
            // lbl_ukupni_rez
            // 
            this.lbl_ukupni_rez.AutoSize = true;
            this.lbl_ukupni_rez.Location = new System.Drawing.Point(483, 80);
            this.lbl_ukupni_rez.Name = "lbl_ukupni_rez";
            this.lbl_ukupni_rez.Size = new System.Drawing.Size(79, 13);
            this.lbl_ukupni_rez.TabIndex = 7;
            this.lbl_ukupni_rez.Text = "ukupni rezultat:";
            this.lbl_ukupni_rez.Click += new System.EventHandler(this.lbl_ukupni_rez_Click);
            // 
            // radiob_X
            // 
            this.radiob_X.AutoSize = true;
            this.radiob_X.Location = new System.Drawing.Point(204, 7);
            this.radiob_X.Name = "radiob_X";
            this.radiob_X.Size = new System.Drawing.Size(32, 17);
            this.radiob_X.TabIndex = 10;
            this.radiob_X.TabStop = true;
            this.radiob_X.Text = "X";
            this.radiob_X.UseVisualStyleBackColor = true;
            this.radiob_X.CheckedChanged += new System.EventHandler(this.radiob_X_CheckedChanged);
            // 
            // radiob_O
            // 
            this.radiob_O.AutoSize = true;
            this.radiob_O.Location = new System.Drawing.Point(242, 7);
            this.radiob_O.Name = "radiob_O";
            this.radiob_O.Size = new System.Drawing.Size(33, 17);
            this.radiob_O.TabIndex = 11;
            this.radiob_O.TabStop = true;
            this.radiob_O.Text = "O";
            this.radiob_O.UseVisualStyleBackColor = true;
            this.radiob_O.CheckedChanged += new System.EventHandler(this.radiob_O_CheckedChanged);
            // 
            // label_prvired
            // 
            this.label_prvired.AutoSize = true;
            this.label_prvired.Location = new System.Drawing.Point(33, 171);
            this.label_prvired.Name = "label_prvired";
            this.label_prvired.Size = new System.Drawing.Size(13, 13);
            this.label_prvired.TabIndex = 12;
            this.label_prvired.Text = "1";
            this.label_prvired.Click += new System.EventHandler(this.label_prvired_Click);
            // 
            // label_drugired
            // 
            this.label_drugired.AutoSize = true;
            this.label_drugired.Location = new System.Drawing.Point(36, 224);
            this.label_drugired.Name = "label_drugired";
            this.label_drugired.Size = new System.Drawing.Size(13, 13);
            this.label_drugired.TabIndex = 13;
            this.label_drugired.Text = "2";
            this.label_drugired.Click += new System.EventHandler(this.label_drugired_Click);
            // 
            // label_trecired
            // 
            this.label_trecired.AutoSize = true;
            this.label_trecired.Location = new System.Drawing.Point(36, 274);
            this.label_trecired.Name = "label_trecired";
            this.label_trecired.Size = new System.Drawing.Size(13, 13);
            this.label_trecired.TabIndex = 14;
            this.label_trecired.Text = "3";
            this.label_trecired.Click += new System.EventHandler(this.label_trecired_Click);
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(121, 139);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(14, 13);
            this.labelA.TabIndex = 15;
            this.labelA.Text = "A";
            this.labelA.Click += new System.EventHandler(this.labelA_Click);
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(238, 138);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(14, 13);
            this.labelB.TabIndex = 16;
            this.labelB.Text = "B";
            this.labelB.Click += new System.EventHandler(this.labelB_Click);
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Location = new System.Drawing.Point(355, 138);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(14, 13);
            this.labelC.TabIndex = 17;
            this.labelC.Text = "C";
            this.labelC.Click += new System.EventHandler(this.labelC_Click);
            // 
            // label_izaberipolje
            // 
            this.label_izaberipolje.AutoSize = true;
            this.label_izaberipolje.Location = new System.Drawing.Point(12, 87);
            this.label_izaberipolje.Name = "label_izaberipolje";
            this.label_izaberipolje.Size = new System.Drawing.Size(65, 13);
            this.label_izaberipolje.TabIndex = 18;
            this.label_izaberipolje.Text = "izaberi polje:";
            this.label_izaberipolje.Click += new System.EventHandler(this.label_izaberipolje_Click);
            // 
            // txtbox_izabranopolje
            // 
            this.txtbox_izabranopolje.Location = new System.Drawing.Point(86, 80);
            this.txtbox_izabranopolje.Name = "txtbox_izabranopolje";
            this.txtbox_izabranopolje.Size = new System.Drawing.Size(100, 20);
            this.txtbox_izabranopolje.TabIndex = 19;
            // 
            // btn_nova_igra
            // 
            this.btn_nova_igra.Location = new System.Drawing.Point(486, 248);
            this.btn_nova_igra.Name = "btn_nova_igra";
            this.btn_nova_igra.Size = new System.Drawing.Size(75, 23);
            this.btn_nova_igra.TabIndex = 20;
            this.btn_nova_igra.Text = "nova igra";
            this.btn_nova_igra.UseVisualStyleBackColor = true;
            this.btn_nova_igra.Click += new System.EventHandler(this.btn_nova_igra_Click);
            // 
            // txtbox_ukupni_prvog
            // 
            this.txtbox_ukupni_prvog.Location = new System.Drawing.Point(486, 136);
            this.txtbox_ukupni_prvog.Name = "txtbox_ukupni_prvog";
            this.txtbox_ukupni_prvog.Size = new System.Drawing.Size(100, 20);
            this.txtbox_ukupni_prvog.TabIndex = 21;
            this.txtbox_ukupni_prvog.TextChanged += new System.EventHandler(this.txtbox_ukupni_prvog_TextChanged);
            // 
            // txtbox_ukupni_drugog
            // 
            this.txtbox_ukupni_drugog.Location = new System.Drawing.Point(486, 206);
            this.txtbox_ukupni_drugog.Name = "txtbox_ukupni_drugog";
            this.txtbox_ukupni_drugog.Size = new System.Drawing.Size(100, 20);
            this.txtbox_ukupni_drugog.TabIndex = 22;
            this.txtbox_ukupni_drugog.TextChanged += new System.EventHandler(this.txtbox_ukupni_drugog_TextChanged);
            // 
            // lbl_ime_prvog_ukupni
            // 
            this.lbl_ime_prvog_ukupni.AutoSize = true;
            this.lbl_ime_prvog_ukupni.Location = new System.Drawing.Point(483, 120);
            this.lbl_ime_prvog_ukupni.Name = "lbl_ime_prvog_ukupni";
            this.lbl_ime_prvog_ukupni.Size = new System.Drawing.Size(53, 13);
            this.lbl_ime_prvog_ukupni.TabIndex = 23;
            this.lbl_ime_prvog_ukupni.Text = "prvi igrac:";
            this.lbl_ime_prvog_ukupni.Click += new System.EventHandler(this.lbl_ime_prvog_ukupni_Click);
            // 
            // lbl_ime_drugog_ukupni
            // 
            this.lbl_ime_drugog_ukupni.AutoSize = true;
            this.lbl_ime_drugog_ukupni.Location = new System.Drawing.Point(483, 190);
            this.lbl_ime_drugog_ukupni.Name = "lbl_ime_drugog_ukupni";
            this.lbl_ime_drugog_ukupni.Size = new System.Drawing.Size(59, 13);
            this.lbl_ime_drugog_ukupni.TabIndex = 24;
            this.lbl_ime_drugog_ukupni.Text = "drugi igrac:";
            this.lbl_ime_drugog_ukupni.Click += new System.EventHandler(this.lbl_ime_drugog_ukupni_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 348);
            this.Controls.Add(this.lbl_ime_drugog_ukupni);
            this.Controls.Add(this.lbl_ime_prvog_ukupni);
            this.Controls.Add(this.txtbox_ukupni_drugog);
            this.Controls.Add(this.txtbox_ukupni_prvog);
            this.Controls.Add(this.btn_nova_igra);
            this.Controls.Add(this.txtbox_izabranopolje);
            this.Controls.Add(this.label_izaberipolje);
            this.Controls.Add(this.labelC);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.label_trecired);
            this.Controls.Add(this.label_drugired);
            this.Controls.Add(this.label_prvired);
            this.Controls.Add(this.radiob_O);
            this.Controls.Add(this.radiob_X);
            this.Controls.Add(this.lbl_ukupni_rez);
            this.Controls.Add(this.lbl_ime_drugog);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btn_pokreni);
            this.Controls.Add(this.txtBox_ime_drugog);
            this.Controls.Add(this.txtBox_ime_prvog);
            this.Controls.Add(this.lbl_ime_prvog);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ime_prvog;
        private System.Windows.Forms.TextBox txtBox_ime_prvog;
        private System.Windows.Forms.TextBox txtBox_ime_drugog;
        private System.Windows.Forms.Button btn_pokreni;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_ime_drugog;
        private System.Windows.Forms.Label lbl_ukupni_rez;
        private System.Windows.Forms.RadioButton radiob_X;
        private System.Windows.Forms.RadioButton radiob_O;
        private System.Windows.Forms.Label label_prvired;
        private System.Windows.Forms.Label label_drugired;
        private System.Windows.Forms.Label label_trecired;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label label_izaberipolje;
        private System.Windows.Forms.TextBox txtbox_izabranopolje;
        private System.Windows.Forms.Button btn_nova_igra;
        private System.Windows.Forms.TextBox txtbox_ukupni_prvog;
        private System.Windows.Forms.TextBox txtbox_ukupni_drugog;
        private System.Windows.Forms.Label lbl_ime_prvog_ukupni;
        private System.Windows.Forms.Label lbl_ime_drugog_ukupni;
    }
}

